import styles from "../../styles/Navbar/MobileNavbar.module.scss"
import { FaTimes, FaBars } from "react-icons/fa"
import ListItemLink from "./ListItemLink"
import { useState } from "react"

const MobileNavBar = () => {

  const [navShow, setNavShow] = useState(false);

  const closeNav = () => {
    setNavShow(false);
  }

  return (
    <div className={styles.container}>
      <div>
        <div className={`${styles.iconContainer} ${styles.bars}`} onClick={() => setNavShow(true)}>
          <FaBars />
        </div>
        <nav className={navShow ? styles.navActive : styles.navInactive}>
          <ul>
            <div className={`${styles.iconContainer} ${styles.times}`} onClick={() => setNavShow(false)}>
              <FaTimes />
            </div>
            <ListItemLink url="" optionClass={styles.linkColor} clickHandler={closeNav}><h3>Home</h3></ListItemLink>
            <ListItemLink url="categories" optionClass={styles.linkColor} clickHandler={closeNav} ><h3>Categories</h3></ListItemLink>
            <ListItemLink url="transaction" optionClass={styles.linkColor} clickHandler={closeNav}><h3>Transactions</h3></ListItemLink>
            <ListItemLink url="wallet" optionClass={styles.linkColor} clickHandler={closeNav}><h3>Wallet</h3></ListItemLink>
            <ListItemLink url="blogs" optionClass={styles.linkColor} clickHandler={closeNav}><h3>Blogs</h3></ListItemLink>
            <div className={styles.mobileMenuLinks}>
              <ListItemLink url="profile" optionClass={styles.linkColor} clickHandler={closeNav}><h3>Profile</h3></ListItemLink>
            </div>
            <div className={styles.mobileMenuLinks}>
              <ListItemLink url="settings" optionClass={styles.linkColor} clickHandler={closeNav}><h3>Settings</h3></ListItemLink>
            </div>
            <ListItemLink url="logout" optionClass={styles.linkColor}><h3>Logout</h3></ListItemLink>
          </ul>
        </nav>
      </div >
    </div >
  )
}

export default MobileNavBar