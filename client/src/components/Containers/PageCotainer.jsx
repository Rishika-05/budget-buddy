import styles from "../../styles/Containers/PageContainer.module.scss"

const PageCotainer = ({children, optionalClass}) => {
  return (
    <div className={`${styles.container} ${optionalClass}`}>{children}</div>
  )
}


PageCotainer.defaultProps = {
    optionalClass: undefined,
}

export default PageCotainer