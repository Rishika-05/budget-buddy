import styles from "../../styles/Cards/TransactionCard.module.scss"
import { FiBox } from 'react-icons/fi'
import { IoGameControllerOutline } from 'react-icons/io5'
import { BsHouseDoor } from 'react-icons/bs'
import { HiOutlineFire } from 'react-icons/hi'
import { RiArrowDownSLine, RiArrowUpSLine } from 'react-icons/ri'
import { useEffect, useState } from 'react';

const CategoryIcon = ({ category }) => {
    const [style, setStyle] = useState({});
    const categoryStyle = () => {
        switch (category) {
            default:
                return {
                    background: "#ffbece",
                    icon: <HiOutlineFire />,
                    color: "#ff6275"
                }
            case "Products":
            case 1: {
                return {
                    background: "#fdeacc",
                    icon: <FiBox />,
                    color: "#f8aa35"
                }
            }
            case "Entertainment":
            case 2: {
                return {
                    background: "#e4f1d5",
                    icon: <IoGameControllerOutline />,
                    color: "#92c44c"
                }
            }
            case "Bills":
            case 3: {
                return {
                    background: "#b7dffd",
                    icon: <BsHouseDoor />,
                    color: "#5a92d6"
                }
            }
        }
    }

    useEffect(() => {
        setStyle(categoryStyle());
    }, []);

    return (
        <div className={styles.iconContainer}
            style={{ background: style.background, color: style.color }}>{style.icon}</div>
    )
}

CategoryIcon.defaultProps = {
    category: "Products"
}

const TransactionCard = ({ category, date, money, description, title }) => {
    const [visible, setVisibile] = useState(false);
    return (
        <div className={styles.container}>
            <div className={styles.inner}>
                <div className={styles.info}>
                    <CategoryIcon category={category} />
                    <div className={styles.categoryContainer}>
                        <span className={styles.title}>{title}</span>
                        <span className={styles.category}>{category}</span>
                        <span className={styles.date}>{date}</span>
                        <div className={`${visible ? styles.descriptionActive : undefined} ${styles.description}`}>
                            <p>{description}</p>
                        </div>
                    </div>
                </div>
                <div className={styles.moneyContainer}>
                    <span>{`-₹${money}`}</span>
                    <div className={styles.iconContainer} onClick={() => setVisibile(!visible)}
                        style={description ? {} : { opacity: 0, pointerEvents: "none" }}>
                        {visible ? <RiArrowUpSLine /> : <RiArrowDownSLine />}
                    </div>
                </div>
            </div>
        </div>
    )
}

TransactionCard.defaultProps = {
    category: "Products",
    date: "29 Feb 2020",
    description: "qwweye hgfshgs rts",
    money: "45.77"
}

export default TransactionCard