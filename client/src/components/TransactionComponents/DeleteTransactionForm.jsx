import styles from '../../styles/TransactionComponents/DeleteTransactionForm.module.scss';
import { Title } from '../Titles/Titles';
import TransactionCard from '../Cards/TransactionCard';
import { BsTrash } from 'react-icons/bs';
import { DateTime } from 'luxon';
import { useState } from 'react';
import { useTransactionGet, useTransactionDelete } from '../../queries/transaction';
import { queryClient } from '../../constants/config';


const DeleteTransactionForm = () => {
    const [firstDate, setFirstDate] = useState(DateTime.now().minus({ day: 1 }).toISODate());
    const [lastDate, setLastDate] = useState(DateTime.now().plus({ day: 1 }).toISODate());
    const { mutate: deleteTransaction, isLoading, isError, isSuccess, error } = useTransactionDelete();
    const { data, refetch: fetchTransactions, isLoading: transactionLoading } = useTransactionGet({ firstDate, lastDate, key: "Trs", });
    return (
        <div className={styles.container}>
            <Title>Delete a Transaction</Title>
            <div className={styles.dateSearchFilter}>
                <div className={styles.date}>
                    <label htmlFor="firstDate">From :</label>
                    <input type="date" id="firstDate" value={firstDate} onChange={(e) => setFirstDate(e.target.value)} />
                </div>
                <div className={styles.date}>
                    <label htmlFor="lastDate">To :</label>
                    <input type="date" id="lastDate" value={lastDate} onChange={(e) => setLastDate(e.target.value)} />
                </div>
                <button className={styles.btn} onClick={() => fetchTransactions()}>
                    Show Transactions
                </button>
            </div>
            <div className={styles.results}>
                {
                    data && data?.map((txn, index) => {
                        return (
                            <div key={index} className={styles.container}>
                                <div className={styles.deleteContainer}>
                                    <TransactionCard
                                        category={txn.category.name}
                                        title={txn.title}
                                        money={txn.money}
                                        date={DateTime.fromISO(txn.date).toISODate()}
                                        description={`${txn.info}`}
                                    />
                                    <div className={styles.iconContainer}
                                        style={transactionLoading ? { pointerEvents: "none", background: "#333" } : {}}
                                        onClick={() => {
                                            deleteTransaction(txn.id, {
                                                onSuccess: async () => {
                                                    await queryClient
                                                        .invalidateQueries("Trs")
                                                        .then(await fetchTransactions())
                                                        .catch();
                                                },
                                            });
                                        }}>
                                        <BsTrash />
                                    </div>
                                </div>
                            </div>
                        )
                    })
                }
            </div>
        </div>
    )
}

export default DeleteTransactionForm