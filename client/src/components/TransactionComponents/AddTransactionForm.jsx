import styles from '../../styles/TransactionComponents/AddTransactionForm.module.scss';
import { Title } from '../Titles/Titles';

import { useEffect, useState } from 'react';
import { useCategoryGet } from '../../queries/category';
import { useTransactionGet, useTransactionPost } from '../../queries/transaction';
import { DateTime } from 'luxon';
import { queryClient } from '../../constants/config';

const AddTransactionForm = () => {
    const [title, setTitle] = useState('');
    const [money, setMoney] = useState(0);
    const [date, setDate] = useState(DateTime.now().toISODate());
    const [category, setCategory] = useState('');
    const [info, setInfo] = useState('');

    const { data: ctgs } = useCategoryGet();

    useEffect(() => {
        if (ctgs) {
            setCategory(ctgs[0].id);
        } else {
            setCategory(1);
        }
    }, [ctgs]);

    const {
        mutate: postTransaction, isLoading, isError, isSuccess, error
    } = useTransactionPost();

    let body = {
        title: title,
        money: parseFloat(money),
        date: date,
        info: info,
        transactionCategoryId: parseInt(category)
    };

    return (
        <div className={styles.container}>
            <Title>Add a Transaction</Title>
            <div className={styles.inner}>
                <input type="text" placeholder="Title" value={title} onChange={(e) => setTitle(e.target.value)} />
                <input type="number" placeholder="Amount" value={money} onChange={(e) => setMoney(e.target.value)} />
                <input type="date" value={date} onChange={(e) => setDate(e.target.value)} />
                <input type="text" placeholder="Description" value={info} onChange={(e) => setInfo(e.target.value)} />
                {
                    ctgs ? (
                        <select value={category} onChange={(e) => setCategory(e.target.value)}>
                            {
                                ctgs.map((ctg) => (
                                    <option key={ctg.id} value={ctg.id}>{ctg.name}</option>
                                ))
                            }
                        </select>
                    ) :
                        (
                            <div>Loading...</div>)
                }
                <button onClick={() =>
                    postTransaction(body, {
                        onSuccess: async () => {
                            await queryClient.invalidateQueries('Categories_Sum');
                        }
                    }
                    )}>
                    {isLoading ? 'Loading...' : 'Add Transaction'}
                </button>
                <div style={{ marginBottom: '1rem'}}>
                    {
                        isError && 
                        error.response.data.map((err, index) => {
                            return (
                                <div key={index} style={{ color: 'red' }}>
                                    {`${err.instancePath}: ${err.message ? err.message : ''}`}
                                </div>
                            )
                        })
                    }
                    {
                        isSuccess && <div style={{ color: 'green' }}>Transaction added successfully!</div>
                    }
                </div>
            </div>
        </div>
    )
}

export default AddTransactionForm