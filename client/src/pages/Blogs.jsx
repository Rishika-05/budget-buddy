import React from 'react'
import { Title } from '../components/Titles/Titles'
import MainContainer from '../components/Containers/MainContainer'
import convert from 'xml-js';
import { useState, useEffect } from 'react'
import { useBlogsGet } from '../queries/blogs'
import '../styles/BlogsComponents/blogs.css'
import api from '../utils/Axios';
import {FINSHOTS_MOCKED_DATA} from '../constants/mock';

let mockedData = false;
const Blogs = () => {
    const [parsedFeeds, setParsedFeeds] = useState([]);
    let finshotRawFeeds = [];

    console.log(parsedFeeds)
    useEffect(() => {
        
        if (!mockedData) {
            api.get('blogs')
                .then((feeds) => {
                    // console.log(feeds.data);
                    prepareParsedFeeds(convert.xml2json(feeds.data));
                }).catch((err) => {
                    console.log(err);
                })
        } else {
            console.log("useEffect called");
            prepareParsedFeeds(convert.xml2json(FINSHOTS_MOCKED_DATA));
        }
    }, []);


    const prepareParsedFeeds = (xmlData) => {
        var json = JSON.parse(xmlData);
        let channel = json.elements[0].elements[0].elements;
        for (let i = 0; i < channel.length; i++) {
            if (channel[i].name === "item")
                finshotRawFeeds.push(channel[i].elements);
        }
        for (var i = 0; i < finshotRawFeeds.length; i++) {
            var parsedFeed = {};
            for (var j = 0; j < finshotRawFeeds[i].length; j++) {
                var temp = finshotRawFeeds[i][j]
                if (temp.name == 'title' || temp.name == 'description' || temp.name == 'link') {
                    parsedFeed[temp.name] = temp.elements[0][temp.elements[0].type];
                }
                if (temp.name === 'media:content') {
                    parsedFeed['imageUrl'] = temp.attributes.url;
                }
            }
            let interimArray = parsedFeeds;
            interimArray.push(parsedFeed);
            setParsedFeeds(interimArray);
            setParsedFeeds([...parsedFeeds]);
        }
    }
    return (
        <MainContainer>
            <Title>Blogs</Title>
            <div className="conatiner" style={{display: 'flex', flexWrap: 'wrap', }}>
                {
                    parsedFeeds.length === 0 && <div>Loading...</div>
                }
                {
                    parsedFeeds.map((key) => {
                        if (key.title && key.description && key.imageUrl && key.link) {
                            return (
                                <a href={key.link} key={key.title} target="_blank">
                                    <div className="e-card">
                                        <div className="e-card-image">
                                            <img src={key.imageUrl} alt="" />
                                        </div>
                                        <div className="e-card-title pl-5 tx-16">{key.title} </div>
                                        <div className="e-card-content pl-5 tx-12">{key.description.substring(0, 60)} </div>
                                    </div>
                                </a>
                            );
                        }
                    })
                }
            </div>
        </MainContainer>
    )
}

export default Blogs