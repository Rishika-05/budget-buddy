import styles from '../styles/ProfileComponents/Profile.module.scss'
import { Title } from '../components/Titles/Titles'
import MainContainer from '../components/Containers/MainContainer'
import { useUser, useUserUpdate } from '../queries/user'
import { useState, useEffect } from 'react'
const Profile = () => {
    const { data: user, isSuccess } = useUser();
    const { mutate: UserUpdate, isSuccess: userUpdated, isError: userNotUpdated, isLoading } = useUserUpdate();
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')

    useEffect(() => {
        if (isSuccess) {
            try {
                setFirstName(user.firstName)
                setLastName(user.lastName)
            } catch (e) { }
        }
    }, [isSuccess])

    let body = {
        firstName,
        lastName
    }
    return (
        <MainContainer>
            <Title>Profile</Title>
            <form action='submit' onSubmit={(e) => e.preventDefault()}>
                <div className={styles.container}>
                    <div className={styles.firstName}>
                        <label htmlFor="firstName">First Name :</label>
                        <input type="text" name="firstName" value={firstName} onChange={(e) => setFirstName(e.target.value)} />
                    </div>
                    <div className={styles.lastName}>
                        <label htmlFor="lastName">Last Name :</label>
                        <input type="text" name="lastName" value={lastName} onChange={(e) => setLastName(e.target.value)} />
                    </div>
                    <button onClick={() => UserUpdate(body, {
                    })}>{isLoading ? "Loading.." : "Update Info"}</button>
                    {userUpdated &&
                        <div style={{ marginTop: '1rem', color: 'green' }}>Success</div>}
                    {userNotUpdated &&
                        <div style={{ marginTop: '1rem', color: 'red' }}>Error</div>}
                </div>
            </form>
        </MainContainer>
    )
}

export default Profile