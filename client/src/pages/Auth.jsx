import styles from '../styles/AuthComponnts/Auth.module.scss';
import MainContainer from '../components/Containers/MainContainer'
import { Title } from '../components/Titles/Titles'
import { useState, useContext, useEffect } from 'react'
import { useNavigate } from 'react-router-dom';
import { useLoginUser, useRegisterUser } from '../queries/user';
import { AuthContext } from '../context/AuthProvider';

const Auth = () => {

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [regEmail, setRegEmail] = useState('');
  const [regPw, setRegPw] = useState('');
  const navigate = useNavigate();
  const {auth, setAuth} = useContext(AuthContext);

  useEffect(() => {
    if (auth)
      navigate('/')
  }, [auth, navigate]);

  let body = {
    email: email,
    password: password
  };

  let regBody = {
    email: regEmail,
    password: regPw
  };

  const { mutate: loginHandler, isError: loginError, error: error } = useLoginUser(body);

  const { mutate: registerHandler, isError: registerError, error: regError } = useRegisterUser(regBody);

  return (
    <MainContainer>
      <form onSubmit={(e) => e.preventDefault()}>
        <div className={styles.container}>
          <Title>Login</Title>
          <span>Email :</span>
          <input type="email" autoComplete='username' value={email} onChange={(e) => setEmail(e.target.value)} />
          <span>Password :</span>
          <input type="password" autoComplete='password' value={password} onChange={(e) => setPassword(e.target.value)} />
          <button onClick={() => {
            loginHandler(body, {
              onError: () => {
                console.log(error)
              }, onSuccess: () => {
                setAuth(true)
              }
            })
          }}>Login</button>
        </div>
        <form onSubmit={(e) => e.preventDefault()} className={styles.registerForm}>
          <div className={styles.container}>
            <Title>Register</Title>
            <span>Email :</span>
            <input type="email" autoComplete='email' value={regEmail} onChange={(e) => setRegEmail(e.target.value)} />
            <span>Password :</span>
            <input type="password" autoComplete='new-password' value={regPw} onChange={(e) => setRegPw(e.target.value)} />
            <button onClick={() => { registerHandler(regBody,{
              onError: () => {
                console.log(regError)
              }, onSuccess: () => {
                loginHandler(regBody, {
                  onError: () => {
                    console.log(error)
                  }, onSuccess: () => {
                    setAuth(true)
                  }
                })
              }
            }) }}>Register Now</button>
          </div>
        </form>
      </form>
    </MainContainer>
  )
}

export default Auth