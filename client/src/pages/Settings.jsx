import styles from '../styles/SettingsComponents/Settings.module.scss'
import { Title } from '../components/Titles/Titles'
import MainContainer from '../components/Containers/MainContainer'

import { useUpdatePassword } from '../queries/user'
import { useState } from 'react'
import { queryClient } from '../constants/config'


const Settings = () => {
    const [oldPassword, setOldPassword] = useState('')
    const [newPassword, setNewPassword] = useState('')
    const { mutate: UpdatePassword, isError, error, isLoading } = useUpdatePassword()
    let body = {
        oldPassword,
        password: newPassword
    }
    return (
        <MainContainer>
            <Title>Settings</Title>
            <form action='submit' onSubmit={(e) => e.preventDefault()}>
                <div className={styles.container}>
                    <Title>Change Password</Title>
                    <div className={styles.password}>
                        <label htmlFor="oldPassword">Current Password :</label>
                        <input type="password" name="oldPassword" autoComplete='current-password' id="oldPassword" value={oldPassword} onChange={(e) => setOldPassword(e.target.value)} />
                    </div>
                    <div className={styles.password}>
                        <label htmlFor="newPassword">New Password :</label>
                        <input type="password" name="newPassword" autoComplete='new-password' value={newPassword} onChange={(e) => setNewPassword(e.target.value)} />
                    </div>
                    <button onClick={() => UpdatePassword(body, {
                        onSuccess: () => {
                            queryClient.invalidateQueries('user');
                            queryClient.removeQueries();
                            setOldPassword('')
                            setNewPassword('')
                        }
                    })}>{isLoading ? "Loading.." : "Change Password"}</button>
                    {isError &&
                        <div style={{ marginTop: '1rem', color: 'red' }}>
                            {error.response.data}
                        </div>}
                </div>
            </form>
        </MainContainer>
    )
}

export default Settings