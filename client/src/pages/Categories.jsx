import styles from '../styles/CategoriesComponents/Categories.module.scss'
import { Title } from '../components/Titles/Titles'
import MainContainer from '../components/Containers/MainContainer'
import TransactionCard from '../components/Cards/TransactionCard'
import { DateTime } from 'luxon'
import { useState, useEffect } from 'react'
import { useCategoryGet } from '../queries/category'
import { useTransactionGet } from '../queries/transaction'

const Categories = () => {
    const [timeSpan, setTimeSpan] = useState(DateTime.now().minus({ days: 7, }).toISODate())
    const [categories, setCategories] = useState([])
    const [sortingFields, setSortingFields] = useState('dateSort')
    const [sortingOrder, setSortingOrder] = useState('asc')
    const { data: ctgs, isFetched: isCtgsFetched } = useCategoryGet()
    const [skip, setSkip] = useState(0)
    const [message, setMessage] = useState('')

    useEffect(() => {
        if (ctgs) {
            setCategories(ctgs[0].id)
        }
    }, [ctgs]);

    const { data: FilteredTransactions, refetch: fetchTransactions, isFetched: fetch } =
        useTransactionGet({
            firstDate: timeSpan,
            category: categories ? categories : undefined,
            [sortingFields]: sortingOrder,
            skip: skip,
            take: 10,
            key: "CategoriesTxns"
        });

    return (
        <MainContainer>
            <Title>Categories</Title>
            <div className={styles.container}>
                <div className={styles.filters}>
                    <div className={styles.filterContainer}>
                        <div className={styles.filter}>
                            <label htmlFor="timeSpan">Time Span</label>
                            <select name="timeSpan" onChange={(e) => setTimeSpan(e.target.value)}>
                                <option value={DateTime.now().minus({ days: 7, }).toISODate()}>Last 7 Days</option>
                                <option value={DateTime.now().minus({ days: 28, }).toISODate()}>Last 28 Days</option>
                                <option value={DateTime.now().minus({ days: 90, }).toISODate()}>Last 90 Days</option>
                                <option value={DateTime.now().minus({ days: 365, }).toISODate()}>Last 365 Days</option>
                            </select>
                        </div>
                    </div>
                    <div className={styles.filterContainer}>
                        <div className={styles.filter}>
                            <label htmlFor="Categories">Categories</label>
                            {isCtgsFetched ? <select name="Categories" onChange={(e) => setCategories(e.target.value)}>
                                {ctgs && ctgs?.map((ctg, index) => {
                                    return (<option key={index} value={ctg.id}>{ctg.name}</option>)
                                })}
                                <option value="">All</option>
                            </select> :
                                <div>Loading...</div>}
                        </div>
                    </div>
                    <div className={styles.filterContainer}>
                        <div className={styles.filter}>
                            <label htmlFor="sortingFields">Sort By</label>
                            <select name="sortingFields" onChange={(e) => setSortingFields(e.target.value)}>
                                <option value="date">Date</option>
                                <option value="price">Amount</option>
                            </select>
                        </div>
                    </div>
                    <div className={styles.filterContainer}>
                        <div className={styles.filter}>
                            <label htmlFor="sortingOrder">Order By</label>
                            <select name="sortingOrder" onChange={(e) => setSortingOrder(e.target.value)}>
                                <option value="asc">Ascending</option>
                                <option value="desc">Descending</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div className={styles.results}>
                    <button className={styles.btn}
                        onClick={(e) => {
                            fetchTransactions();
                            if(FilteredTransactions.length == 0) {
                                setMessage('No transactions found')
                            }else{
                                setMessage('')
                            }
                        }}>
                        Show Results</button>
                    <div className={styles.inner}>
                        {
                            fetch && FilteredTransactions && FilteredTransactions.length == 0 && <div>{message}</div>
                        }
                        {FilteredTransactions && FilteredTransactions.map((txn, index) => {
                            return (
                                <TransactionCard
                                    key={index}
                                    id={txn.id}
                                    title={txn.title}
                                    date={DateTime.fromISO(txn.date).toISODate()}
                                    money={txn.money}
                                    category={txn.category.name}
                                    description={txn.info}
                                />
                            )
                        })}
                    </div>
                </div>
            </div>
        </MainContainer>
    )
}

export default Categories