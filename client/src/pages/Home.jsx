import styles from '../styles/HomeComponents/Home.module.scss'
import MainContainer from '../components/Containers/MainContainer'
import { Title } from '../components/Titles/Titles'
import Searchbar from '../components/HomeComponents/Searchbar'
import CategoryCard from '../components/Cards/CategoryCard'
import TransactionCard from '../components/Cards/TransactionCard'
import HomeProfile from '../components/HomeComponents/HomeProfile'
import { useTransactionGet } from '../queries/transaction'
import { useCategorySumGet, useCategoryGet } from '../queries/category'
import { useEffect } from 'react'
import { DateTime } from 'luxon'
const Home = () => {

  const { data: transactions, refetch: fetchTransactions } = useTransactionGet({
    key: "Txns_Latest",
    ['dateSort']: 'desc',
    take: 5,
    skip: 0,
  });

  const { data: CategoriesSum ,  refetch: fetchCategoriesSum } = useCategorySumGet();
  const { data: ctgs, isFetched: isCtgsFetched } = useCategoryGet()


  useEffect(() => {
    fetchTransactions();
    fetchCategoriesSum();
  }, []);

  return (
    <MainContainer optionClass={styles.container}>
      <div className={styles.main}>
        <div className={styles.searchbar}>
          <Searchbar />
        </div>

        <div className={styles.categories}>
          <Title>Categories Last 30 Days</Title>
          <div className={styles.content}>
            {isCtgsFetched && CategoriesSum && CategoriesSum?.map((category, index) => {
              console.log(category)
              return (
                <CategoryCard
                  key={index}
                  category={ctgs[category.transactionCategoryId - 1].name}
                  money={category._sum.money} />
              )
            })}
          </div>
          <div className={styles.transactions}>
            <Title>Latest Transactions</Title>
            <div className={styles.content}>
              {transactions && transactions.map((txn, index) => {
                return (
                  <TransactionCard
                    key={index}
                    id={txn.id}
                    date={DateTime.fromISO(txn.date).toISODate()}
                    money={txn.money}
                    title={txn.title}
                    category={txn.category.name}
                    description={txn.info}
                  />
                )
              })}
            </div>
          </div>
        </div>
      </div>
      <div className={styles.profile}>
        <HomeProfile />
      </div>
    </MainContainer>
  )
}

export default Home