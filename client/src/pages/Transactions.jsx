import styles from '../styles/TransactionComponents/Transactions.module.scss';
import { Title } from '../components/Titles/Titles';
import MainContainer from '../components/Containers/MainContainer';
import AddTransactionForm from '../components/TransactionComponents/AddTransactionForm';
import DeleteTransactionForm from '../components/TransactionComponents/DeleteTransactionForm';
import { useState } from 'react';

const Transactions = () => {
    const [addTxn, setAddTxn] = useState(true);
    return (
        <MainContainer>
            <Title>Transactions</Title>
            <div className={styles.tabbar}>
                <span className={addTxn ? styles.activeTab : styles.tab} onClick={() => setAddTxn(true)}>Add Transaction</span>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <span className={!addTxn ? styles.activeTab : styles.tab} onClick={() => setAddTxn(false)}>Delete Transaction</span>
            </div>
            {addTxn ? <AddTransactionForm /> :
                <DeleteTransactionForm />
            }
        </MainContainer>
    )
}

export default Transactions