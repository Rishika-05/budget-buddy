import styles from '../styles/WalletComponents/Wallet.module.scss'
import MainContainer from '../components/Containers/MainContainer'
import { Title } from '../components/Titles/Titles'
import { useTransactionGet } from '../queries/transaction'
import { useEffect, useState } from 'react'

const Wallet = () => {
    const date = new Date()
    const firstDay = new Date(date.getFullYear(), date.getMonth(), 1)
    const [Amount, setAmount] = useState(0)
    const { data: transactions, refetch: fetchTransactions } = useTransactionGet({
        key: "Txns_Latest",
        firstDate: firstDay.toISOString(),
        lastDate: date.toISOString(),
        skip: 0,
    });

    useEffect(() => {
        fetchTransactions();
    }, []);

    useEffect(() => {
        let sum = 0
        transactions && transactions.map((txn) => {
            sum += txn.money
        })
        setAmount(sum)
    }, [transactions])

    return (
        <MainContainer>
            <Title>Wallet</Title>
            <div className={styles.container}>
                <div className={styles.inner}>
                    <p className={styles.title}>Total Expenditure (Current month)</p>
                    <p className={styles.amount}>₹{Amount}</p>
                </div>
            </div>
        </MainContainer>
    )
}

export default Wallet