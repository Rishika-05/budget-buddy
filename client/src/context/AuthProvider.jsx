import { createContext, useState, useEffect } from "react";
import { queryClient } from "../constants/config";
import { useUser } from "../queries/user";
import {useNavigate} from 'react-router-dom';

const AuthContext = createContext({
    auth: false,
    setAuth: () => {},
});

const AuthProvider = ({ children }) => {
    const [auth, setAuth] = useState(false);
    const { data, isError } = useUser();
    const navigate = useNavigate();

    useEffect(() => {
        if (data?.userId) {
            setAuth(true);
        }
        if(isError) {
            setAuth(false);
            queryClient.removeQueries()
        }
    }, [data, isError, navigate]);

    return (
        <AuthContext.Provider value={{ auth, setAuth }}>
            {children}
        </AuthContext.Provider>
    );
}

export { AuthContext, AuthProvider };
