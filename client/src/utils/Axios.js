import axios from 'axios';
import { AXIOS_URL } from '../constants/config';

const api = axios.create({
    baseURL: AXIOS_URL,
    withCredentials: false,
});

export default api;