import './styles/App.scss';
import PageCotainer from './components/Containers/PageCotainer';
import Navbar from './components/Navbar/Navbar';
import MobileNavBar from './components/Navbar/MobileNavBar';
import Auth from './pages/Auth';
import Home from './pages/Home';
import Settings from './pages/Settings';
import Profile from './pages/Profile';
import Transactions from './pages/Transactions';
import Categories from './pages/Categories';
import Blogs from './pages/Blogs';
import Wallet from './pages/Wallet';
import { Routes, Route
 } from 'react-router-dom';
import MainContainer from './components/Containers/MainContainer';
import ProtectedRoutes from './components/ProtectedRoutes';
import { AuthProvider } from './context/AuthProvider';
import { ReactQueryDevtools } from 'react-query/devtools';
import { QueryClientProvider } from 'react-query';
import { queryClient } from './constants/config';

function App() {
  return (
    <div className="App">
      <QueryClientProvider client={queryClient}>
        <AuthProvider>
          <PageCotainer optionalClass={"pageContainer"}>
            <Navbar />
            <div className="mobileMenu">
              <MobileNavBar />
            </div>
            <Routes>
              <Route>
                <Route path="/auth" element={<Auth />} />
                <Route element={<ProtectedRoutes />}>
                  <Route path="/" element={<Home />} />
                  <Route path="/settings" element={<Settings />} />
                  <Route path="/profile" element={<Profile />} />
                  <Route path="/transaction" element={<Transactions />} />
                  <Route path="/categories" element={<Categories />} />
                  <Route path="/blogs" element={<Blogs />} />
                  <Route path="/wallet" element={<Wallet />} />
                  <Route path="/*" element={
                    <MainContainer>
                      <span style={{ fontSize: '1.2rem' }}>404 Page Not Found</span>
                    </MainContainer>
                  } />
                </Route>
              </Route>
            </Routes>
          </PageCotainer>
        </AuthProvider>
        {/* <ReactQueryDevtools /> */}
      </QueryClientProvider>
    </div>
  );
}

export default App;
