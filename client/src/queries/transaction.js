import { useQuery, useMutation } from 'react-query';
import api from '../utils/Axios';

const deleteTxn = async (params) => {
    const { data } = await api.delete(`transaction/delete/${params}`, { data: { userId: Number(localStorage.getItem('user_id')) } });
    return data;
}

const fetchTxn = async (params) => {
    const { data } = await api.post('transactions', { userId: Number(localStorage.getItem('user_id')) }, { params: params })
        .catch((err) => {
            console.log(err)
        });
    return data;
}

const addTxn = async (params) => {
    const { data } = await api.post('transaction', { ...params, userId: Number(localStorage.getItem('user_id')) });
    return data;
}

const useTransactionDelete = () => {
    return useMutation("deleteTxn", deleteTxn);
}

const useTransactionGet = ({ firstDate, lastDate, category, dateSort, priceSort, skip, take, key }) => {
    return useQuery(key,
        () =>
            fetchTxn({ firstDate, lastDate, category, dateSort, priceSort, skip, take }), {
        refetchOnWindowFocus: false,
        enabled: false,
        keepPreviousData: true,
    }
    )
}

const useTransactionPost = () => {
    return useMutation("addTxn", addTxn);
}

export { useTransactionDelete, useTransactionGet, useTransactionPost };