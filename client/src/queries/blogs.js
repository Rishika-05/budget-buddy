import { useQuery, useMutation } from 'react-query'
import api from '../utils/Axios'

const fetchBlogs = async () => {
    const data  = await api.get('blogs');
    return data;
}

const useBlogsGet = () => {
    return useMutation("getBlogs", fetchBlogs);
}

export { fetchBlogs, useBlogsGet } 