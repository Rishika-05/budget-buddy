import { useQuery, useMutation } from 'react-query'
import api from '../utils/Axios'

const fetchUser = async () => {
    const { data } = await api.post('whoami', { userId: Number(localStorage.getItem('user_id')) });
    return data;
}

const registerUser = async (req) => {
    console.log(req);
    const { data } = await api.post('register', req);
    return data;
}

const loginUser = async (req) => {
    const { data } = await api.post('login', req);
    localStorage.setItem('user_id', data.userId)
    return data;
}

const logoutUser = async () => {
    const { data } = await api.post('logout', { userId: Number(localStorage.getItem('user_id')) });
    localStorage.clear();
    return data;
}

const userUpdate = async (body) => {
    const { data } = await api.post('me', { ...body, userId: Number(localStorage.getItem('user_id')) });
    return data;
}

const updatePassword = async (body) => {
    const { data } = await api.post('me/pw', { ...body, userId: Number(localStorage.getItem('user_id')) });
    return data;
}

const useUser = () => {
    return useQuery('user', fetchUser, {
        refetchOnWindowFocus: true,
        retry: false,
    });
}
const useLoginUser = () => {
    return useMutation("loginUser", loginUser);
}
const useLogutUser = () => {
    return useMutation("logoutUser", logoutUser);
}
const useRegisterUser = () => {
    return useMutation("registerUser", registerUser);
}
const useUserUpdate = () => {
    return useMutation("userUpdate", userUpdate);
}
const useUpdatePassword = () => {
    return useMutation("updatePassword", updatePassword);
}
export { useUser, useLoginUser, useLogutUser, useRegisterUser, useUserUpdate, useUpdatePassword };