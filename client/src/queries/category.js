import {useQuery} from 'react-query';
import api from '../utils/Axios';

const fetchCategory = async () => {
    const {data} = await api.post('categories', {userId: Number(localStorage.getItem('user_id'))});
    return data;
}

const fetchCategorySum = async () => {
    const {data} = await api.post('categories/sum', {userId: Number(localStorage.getItem('user_id'))});
    return data;
}

const useCategoryGet = () => {
    return useQuery('Categories', fetchCategory, {
        staleTime: 50000,
    });
}

const useCategorySumGet = () => {
    return useQuery('Categories_Sum', fetchCategorySum, {
        staleTime: 30000,
    });
}

export {useCategoryGet, useCategorySumGet};