const express = require('express')
const session = require('express-session')
const { PrismaSessionStore } = require('@quixo3/prisma-session-store');
const { PrismaClient } = require('@prisma/client')
const cors = require('cors')
const path = require('path')

const authRoutes = require('./routes/authRoutes')
const userRoutes = require('./routes/userRoutes')
const transactionRoutes = require('./routes/transactionRoutes')
const categoryRoutes = require('./routes/categoriesRoutes')
const blogRoutes = require('./routes/blogRoutes')

const app = express()
const port = process.env.PORT || 5000
const prisma = require('./constants/config')

app.use(cors({
    origin: ['http://localhost:3000', 'https://localhost:5000'],
    methods: ['GET', 'HEAD', 'PUT', 'PATCH', 'POST', 'DELETE'],
    credentials: true
}));

app.use(session({
    name: 'session',
    secret: 'secret',
    resave: false,
    saveUninitialized: false,
    store: new PrismaSessionStore(prisma, {
        checkPeriod: 1000 * 60 * 60 * 24,  //ms
        dbRecordIdIsSessionId: true,
        dbRecordIdFunction: undefined,
    }),
    cookie: {
        maxAge: 1000 * 60 * 60 * 24, // 1 day
        secure: false,
    },
}))


app.use(express.json())
app.use(express.urlencoded({ extended: true }))

app.use('/api', authRoutes)
app.use('/api', userRoutes)
app.use('/api', transactionRoutes)
app.use('/api', categoryRoutes)
app.use('/api', blogRoutes)

app.listen(port, () => {
    console.log(`Server is running on port ${port}`)
})