const {prisma} = require("../constants/config");

const seed = async () => {
    try{
        let ctgs = await prisma.transactionCategory.findMany();
        if(ctgs.length === 0){
            console.log("Seeding categories");
            await prisma.transactionCategory.createMany({
                data: [
                    {name: "Products"},
                    {name: "Bills"},
                    {name: "Entertainment"},
                    {name: "Other"},
                ]
            }).catch((err) => console.log("error seeding categories"));
        }
    }catch(err){
        console.log("error seeding categories");
    }
};


seed();