const express = require('express');
const categoriesController = require('../controllers/categoryController');
const router = express.Router();

router.post('/categories', categoriesController.categories_get);
router.post('/categories/sum', categoriesController.categories_transaction_sum);

module.exports = router;