const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');

router.post('/me', userController.user_update);
router.post('/me/pw', userController.update_password);


module.exports = router;