const { prisma } = require('../constants/config');
const bcrypt = require('bcrypt');

const user_update = async (req, res) => {
    const { firstName, lastName, userId } = req.body;
    if (userId) {
        const user = await prisma.user.update({
            where: {
                id: userId
            },
            data: {
                firstName: firstName,
                lastName: lastName
            }
        })
        res.status(200).send("User updated.");
    } else {
        res.status(401).send("Please login.");
    }
}

const update_password = async (req, res) => {
    let user;
    const { userId, oldPassword, password } = req.body;
    console.log(req.body);
    if (userId) {
        try {
            user = await prisma.user.findUnique({
                where: {
                    id: userId
                },
            });
        } catch (err) {
            console.log(err);
            res.status(500).send("Something went wrong.");
            return;
        }
    } else {
        res.status(401).send("Please login.");
    }

    if (user) {
        const passwordMatch = await bcrypt.compare(oldPassword, user.password);
        if (passwordMatch) {
            const saltRounds = 10;
            const salted_password = await bcrypt.hash(password, saltRounds);
            try {
                await prisma.user.update({
                    where: {
                        id: userId
                    },
                    data: {
                        password: salted_password
                    }
                });
                try {
                    await prisma.session.deleteMany({
                        where: {
                            data: {
                                endsWith: `,"userId":"${userId}"}}`
                            },
                        },
                    });
                    res.clearCookie("session").status(200).send("Updated password.");
                } catch {
                    res.status(500).send("Something went wrong in session.");
                }
            } catch (err) {
                console.log(err);
                res.status(500).send("Something went wrong.");
                return;
            }
        } else {
            res.status(403).send("Please enter correct credentials.");
        }
    } else {
        res.status(401).send("Please login.");
    }
};

module.exports = { user_update, update_password }