const { prisma } = require('../constants/config');
const { DateTime } = require('luxon');

const categories_get = async (req, res) => {
    let userId = req.body.userId;
    if (!userId) {
        res.status(401).send("Not logged in");
        return;
    }
    if (userId) {
        let categories;
        try {
            categories = await prisma.transactionCategory.findMany()
                .catch((err) => console.log(err));
            if (categories) {
                res.status(200).send(categories);
            }
        } catch (error) {
            res.status(400).send([{ instancePath: 'Categories', message: "error" }]);
        }
    }
}

const categories_transaction_sum = async (req, res) => {
    let userId = req.body.userId;
    if (!userId) {
        res.status(401).send("Not logged in");
        return;
    }
    if (userId) {
        let firstDate = req.query.firstDate;
        let lastDate = DateTime.now().toISO();
        if (!firstDate) {
            firstDate = DateTime.now().minus({ days: 30 }).toISO();
        }
        try {
            const transactions = await prisma.transaction.groupBy({
                by: ['transactionCategoryId'],
                where: {
                    wallet: {
                        userId: userId,
                    },
                    date: {
                        gte: firstDate,
                        lte: lastDate
                    },
                },
                _sum: {
                    money: true
                },
            });
            res.status(200).send(transactions);
        } catch {
            res.status(400).send([{ instancePath: 'Categories_Sum', message: "error" }]);
        }
    }
}


module.exports = {
    categories_get,
    categories_transaction_sum,
};