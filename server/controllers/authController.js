const { prisma } = require('../constants/config')
const bcrypt = require('bcrypt')

const register = async (req, res) => {
    const { email, password } = req.body
    let emailCheck;
    try {
        emailCheck = await prisma.user.findUnique({
            where: {
                email
            }
        });
    } catch (error) {
        res.status(400).send([{ instancePath: 'Email Availability', message: "error" }])
    }
    if (emailCheck) {
        res.status(500).json([{ instancePath: 'Email', message: "Email is already taken" }])
    } else {
        const saltRounds = 10;
        const salted_password = await bcrypt.hash(password, saltRounds);
        let newUser;
        try {
            newUser = await prisma.user.create({
                data: {
                    email,
                    password: salted_password
                }
            });
        } catch (error) {
            res.status(500).send([{ instancePath: 'Register', message: "error" }]);
            return;
        }
        try {
            await prisma.wallet.create({
                data: {
                    userId: newUser.id,
                }
            })
            res.status(200).send('ok');
        } catch (error) {
            res.status(400).send([{ instancePath: 'Register wallet', message: "error" }]);
            return;
        }
    }
};

const login = async (req, res) => {
    console.log("login")
    // console.log(req.body);
    let userId = req.body.userId;
    if (userId) {
        res.status(500).send("Already logged in");
        return;
    }
    let user;
    const { email, password } = req.body;
    try {
        user = await prisma.user.findUnique({
            where: {
                email
            }
        });
    } catch (error) {
        if (!user) {
            res.status(401).send("Please enter correct credentials");
            return;
        }
    }

    const passwordCheck = await bcrypt.compare(password, user.password);
    if (passwordCheck) {
        req.session.userId = user.id;
        res.status(200).send({ userId: user.id });
    } else {
        res.status(401).send("Please enter correct credentials");
    }
    console.log(req.session.userId);
};

const logout = async (req, res) => {
    let userId = req.body.userId;
    if (userId) {
        req.session.destroy();
        res.clearCookie("session").status(200).send("Cookie cleared");
    } else {
        res.status(401).send("You are not logged in");
    }
}

const user = async (req, res) => {
    // console.log("user")
    // console.log(req.body);
    let userId = req.body.userId;
    if (userId) {
        try {
            const user = await prisma.user.findUnique({
                where: {
                    id: Number(userId)
                }
            });
            if(!user) {
                res.status(401).send("User not found");
            }
            const data = {
                email: user.email,
                userId: user.id,
                firstName: user.firstName,
                lastName: user.lastName,
            }
            res.status(200).send(data);
        } catch (error) {
            res.status(500).json("something went wrong {user}");
        }
    } else {
        res.status(401).send("Please login");
    }
}

module.exports = {
    register, login, logout, user
}