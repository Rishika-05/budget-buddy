const { prisma } = require('../constants/config');
const { DateTime } = require('luxon');


const transaction_post = async (req, res) => {
    let userId = req.body.userId;
    if (!userId) {
        res.status(401).send("Not logged in");
        return;
    }
    if (userId) {
        const date = new Date(req.body.date).toISOString();
        const wallet = await prisma.wallet.findUnique({
            where: {
                userId: userId
            },
        }).catch();
        try {
            await prisma.transaction.create({
                data: {
                    date: date,
                    money: req.body.money,
                    title: req.body.title,
                    info: req.body.info,
                    walletId: wallet.id,
                    transactionCategoryId: req.body.transactionCategoryId,
                }
            })
            res.status(200).send('Success!!');
        } catch (error) {
            res.status(400).send([{ instancePath: 'Transaction', message: "error" }]);
            return;
        }
    }
}


const transaction_get = async (req, res) => {
    let userId = req.body.userId;
    if (!userId) {
        res.status(401).send("Not logged in");
        return;
    }
    if (userId) {
        let { firstDate, lastDate, category, dateSort, priceSort, take, skip } = req.query;
        if (!Number(skip)) {
            skip = 0;
        }
        if (!Number(take)) {
            take = 5;
        }
        const transactions = await prisma.transaction.findMany({
            where: {
                wallet: {
                    userId: userId
                },
                date: {
                    gte: firstDate != undefined ? DateTime.fromISO(firstDate).toISO()
                        : DateTime.now().minus({ days: 30 }).toISO(),
                    lte: lastDate != undefined ? DateTime.fromISO(lastDate).toISO() :
                        DateTime.now().toISO()
                },
                transactionCategoryId: {
                    equals: category != undefined ? parseInt(category) : undefined
                },
            },
            skip: parseInt(skip),
            take: parseInt(take),
            orderBy: {
                date: dateSort != undefined ? dateSort : undefined,
                money: priceSort != undefined ? priceSort : undefined,
            },
            select: {
                title: true,
                money: true,
                date: true,
                info: true,
                id: true,
                category: {
                    select: {
                        name: true,
                    },
                },
            },
        }).catch((e) => {
            console.log(e);
            res.status(400).send("error");
        });
        res.json(transactions);
    }
}


const transaction_delete = async (req, res) => {
    let userId = req.body.userId;
    if (!userId) {
        res.status(401).send("Not logged in");
        return;
    }
    if (userId) {
        let transactionId = parseInt(req.params.transactionId);
        let tr;
        try {
            tr = await prisma.transaction.deleteMany({
                where: {
                    id: transactionId,
                    wallet: {
                        userId: userId
                    },
                },
            })
            res.status(200).send('Success!!');
        } catch (error) {
            res.status(500).send("Somthing went wrong with deletion of the particular transaction");
            return;
        }
        if (tr?.count) {
            res.status(200).send("Success!!");
            return;
        }
        res.status(400).send("Transaction not found");
    }
};

module.exports = {
    transaction_post,
    transaction_get,
    transaction_delete,
}